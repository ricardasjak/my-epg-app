# Insight Challenge!

## Overview

The aim of this challenge is to get a feeling of your coding style and capabilities.
We recommend to spend around 4 hours in this exercise.
Please ask any questions you may have about the exercise.

Complete the exercise in React, and keep in mind that you may use any libraries or tools (i.e., create-react-app, axios, etc) of your preference.

## Description

Build a client application that allows a user to define which programs he/she wants to record on a given channel for a given day.

- Electronic Program Guide (EPG) is to be obtained from a webservice with the following endpoint: https://s3-eu-west-1.amazonaws.com/jba-test/epg.json
- The EPG contains the programs for just 1 channel, and for just 1 day (24 hours)
- The user must be able to see the information for each of the programs on the channel
- The user must be able to select which of these programs he/she wants to record
- A summary of the selected programs should be displayed, including the total number of items to record, the total size of the data, as well as the total amount of time
- There should be button to print in the console the JSON that you would send to a theoretical API that would make the actual recordings.

## Delivery

Submit your code to the Git repository provided in the email.
