## About
This is react-redux demo app created using
[create-react-app](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md)

[Task description](./front_end_challenge.md)

## Start in dev mode
* yarn install
* yarn start
* yarn test (optional)

## Deployment
* yarn build (it creates production-ready bundle in "build" folder)

##Author
Ricardas Jaksebaga

