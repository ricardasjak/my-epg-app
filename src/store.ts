import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from 'redux-thunk';
import epgStore, {EpgStoreType} from './Epg/epgStore';

export interface RootStoreType {
  epgStore: EpgStoreType
}

const rootReducer = combineReducers({
  epgStore
});

export default function configureStore() {
  return createStore(
    rootReducer,
    applyMiddleware(thunk)
  );
}
