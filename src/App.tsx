import * as React from 'react';
import './App.css';
import EpgPage from "./Epg/EpgPage";

const App = () => (
  <div className="app">
    <header>
      <h1>Welcome to My EPG Manager</h1>
      <EpgPage/>
    </header>
  </div>
);

export default App;
