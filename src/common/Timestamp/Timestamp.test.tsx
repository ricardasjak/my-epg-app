import * as enzyme from 'enzyme';
import * as React from 'react';
import Timestamp from './Timestamp';

test('Timestamp -- should render', () => {
  const date = new Date(2000, 0, 1, 17, 1, 0);
  const timestamp = enzyme.shallow(<Timestamp date={date}/>);
  expect(timestamp.html()).toEqual('<div>17:01</div>');
});
