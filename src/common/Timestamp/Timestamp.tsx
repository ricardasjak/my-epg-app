import * as React from 'react';

interface TimestampProps {
  date: Date
}

const convertToTwoDigitsString = (value: number): string =>
  value > 9 ? `${value}` : `0${value}`;

const Timestamp: React.StatelessComponent<TimestampProps> = ({date}) => {

  const hours = date.getHours();
  const minutes = date.getMinutes();

  return (
    <div>
      {`${convertToTwoDigitsString(hours)}:${convertToTwoDigitsString(minutes)}`}
    </div>
  )
};

export default Timestamp;
