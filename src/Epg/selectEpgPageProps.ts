import {RootStoreType} from '../store';
import {EpgPageProps} from './EpgPage';
import selectElectronicProgramGuide from './selectElectronicProgramGuide';
import selectElectronicProgramGuideSummary from './selectElectronicProgramGuideSummary';

// todo: add tests
const selectEpgPageProps = (state: RootStoreType, props: any): EpgPageProps => {
  const electronicProgramGuide = selectElectronicProgramGuide(state, props);
  if (electronicProgramGuide === undefined) {
    return {};
  }
  return {
    electronicProgramGuide,
    electronicProgramGuideSummary: selectElectronicProgramGuideSummary(electronicProgramGuide.programs)
  };
};

export default selectEpgPageProps;
