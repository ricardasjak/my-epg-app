import * as epgApi from '../api/epgApi';
import {RootStoreType} from '../store';

// todo: lack of TS support on action creators
// todo: check // https://github.com/piotrwitek/react-redux-typescript-guide#action-creators

export interface EpgStoreType {
  electronicProgramGuide?: ElectronicProgramGuide,
  selectedPrograms: string[]
}

const epgActionTypes = {
  EPG_LOAD: 'EPG_LOAD',
  EPG_LOADED: 'EPG_LOADED',
  EPG_PROGRAM_SELECTION_CHANGED: 'EPG_PROGRAM_SELECTION_CHANGED'
};

/* action creators */
const epgStartLoadAction = () => ({
  type: epgActionTypes.EPG_LOAD
});

export const epgLoadAction = () => {
  // @ts-ignore
  return (dispatch) => {
    dispatch(epgStartLoadAction());
    epgApi.getElectronicProgramGuide()
      .then(epg => dispatch(epgLoadedAction(epg)));
  };
};

export const epgSubmitSelectionAction = () => {
  // @ts-ignore
  return (dispatch, getState) => {
    const state = getState() as RootStoreType;
    return epgApi.submitWishList(state.epgStore.selectedPrograms)
  };
};

export const epgLoadedAction = (payload: ElectronicProgramGuide) => ({
  payload,
  type: epgActionTypes.EPG_LOADED,
});

export const epgProgramSelectionChangedAction = (payload: string) => ({
  payload,
  type: epgActionTypes.EPG_PROGRAM_SELECTION_CHANGED,
});

/* reducers */
// @ts-ignore
export default (state: EpgStoreType = {selectedPrograms: []}, {type, payload}) => {
  const newState: EpgStoreType = {
    ...state,
    selectedPrograms: [...state.selectedPrograms]
  };
  switch (type) {
    case epgActionTypes.EPG_LOAD: {
      newState.electronicProgramGuide = undefined;
      return newState;
    }
    case epgActionTypes.EPG_LOADED: {
      newState.electronicProgramGuide = {...payload};
      return newState;
    }
    case epgActionTypes.EPG_PROGRAM_SELECTION_CHANGED: {
      const id = payload as string;
      const i = newState.selectedPrograms.indexOf(id);
      if (i === -1) {
        newState.selectedPrograms.push(id);
      } else {
        newState.selectedPrograms.splice(i, 1);
      }
      return newState;
    }
    default:
      return newState;
  }
}
