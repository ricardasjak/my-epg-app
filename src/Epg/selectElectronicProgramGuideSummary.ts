// todo: add tests
const selectElectronicProgramGuideSummary = (programs: Program[]): ElectronicProgramGuideSummary => {

  const summary: ElectronicProgramGuideSummary = programs
    .filter(program => !!program.selected)
    .reduce((result: ElectronicProgramGuideSummary, program) => {
        result.selectedProgramsCount++;
        result.totalSize += program.fileSize;
        result.totalTime += (program.end - program.start);
        return result;
      }, {
      selectedProgramsCount: 0,
      totalSize: 0,
      totalTime: 0,
    });

  return summary;
};

export default selectElectronicProgramGuideSummary;
