import * as React from 'react';
import {connect} from 'react-redux';

import EpgList from './EpgList/EpgList';
import {epgLoadAction, epgProgramSelectionChangedAction, epgSubmitSelectionAction} from './epgStore';
import EpgSummary from './EpgSummary/EpgSummary';
import selectEpgPageProps from './selectEpgPageProps';

export interface EpgPageProps {
  electronicProgramGuide?: ElectronicProgramGuide,
  electronicProgramGuideSummary?: ElectronicProgramGuideSummary
}

class EpgPage extends React.Component<EpgPageProps> {

  public componentDidMount() {
    // There are at least 4 ways how to access actions from React-Redux container components
    // I chose simplest approach.

    // @ts-ignore
    this.props.dispatch(epgLoadAction());
  }

  public render() {
    const {electronicProgramGuide, electronicProgramGuideSummary} = this.props;
    const hasSomeSelection = electronicProgramGuideSummary && electronicProgramGuideSummary.selectedProgramsCount > 0;
    return (
      <div className="epg-page">
        <h2>Electronic Program Guide</h2>
        <EpgList
          electronicProgramGuide={electronicProgramGuide}
          onProgramSelection={this.handleProgramSelection}
        />
        <EpgSummary
          electronicProgramGuideSummary={electronicProgramGuideSummary}
        />
        {hasSomeSelection &&
          <button onClick={this.handleSubmitSelection}>Submit selection</button>
        }
      </div>
    );
  }

  private handleProgramSelection = (id: string) => {
    // @ts-ignore
    this.props.dispatch(epgProgramSelectionChangedAction(id));
  };

  private handleSubmitSelection = () => {
    // @ts-ignore
    this.props.dispatch(epgSubmitSelectionAction())
      .then(() => alert('Success! Look at console for payload.'))
      // @ts-ignore
      .catch(err => alert(`Failure. ${err.message}`));
  }
}

export default connect(selectEpgPageProps)(EpgPage);
