import {RootStoreType} from '../store';

// todo: add tests
const selectElectronicProgramGuide = (state: RootStoreType, props: any): ElectronicProgramGuide | undefined => {
  const {
    electronicProgramGuide,
    selectedPrograms
  } = state.epgStore;

  if (electronicProgramGuide === undefined) {
    return undefined;
  }

  const {channelName, date, programs} = electronicProgramGuide;

  return {
    channelName,
    date,
    programs: programs.map(program => ({
        ...program,
        selected: (selectedPrograms.indexOf(program.id) > -1)
      })
    )
  }
};

export default selectElectronicProgramGuide;
