import * as React from 'react';

import './EpgSummary.css';

interface EpgSummaryProps {
  electronicProgramGuideSummary?: ElectronicProgramGuideSummary
}

const unknownDuration = 'unknown';

// todo: add tests
class EpgSummary extends React.PureComponent<EpgSummaryProps> {

  public render() {
    if (this.props.electronicProgramGuideSummary === undefined) {
      return null;
    }
    const {selectedProgramsCount, totalSize, totalTime} = this.props.electronicProgramGuideSummary;
    return (
      <div className="epg-summary">
        <div className="epg-summary-row">
          {this.labelCell('Selected programs')}
          {this.valueCell(selectedProgramsCount)}
        </div>
        <div className="epg-summary-row">
          {this.labelCell('Total size')}
          {/*todo: extract to separate stateless component, make it nicer */}
          {this.valueCell(`${Math.ceil(totalSize / 1024 / 1024)} MB`)}
        </div>
        <div className="epg-summary-row">
          {this.labelCell('Total time')}
          {/*todo: extract to separate stateless component, make it nicer */}
          {this.valueCell(this.totalTimeValue(totalTime))}
        </div>
      </div>
    );
  }

  private labelCell = (label: string) => (<span className="epg-summary-row-label">{label}</span>);
  private valueCell = (value: string | number) => (<span className="epg-summary-row-value">{value}</span>);
  private totalTimeValue = (totalTime: number): string =>
    isNaN(totalTime) ? unknownDuration : `${Math.ceil(totalTime / 1000 / 60)} min`;
}

export default EpgSummary;
