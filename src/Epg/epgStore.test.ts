import {epgLoadedAction, epgProgramSelectionChangedAction, EpgStoreType} from './epgStore';
import epgStore from './epgStore';

test('epgLoadedAction -- should create action', () => {

  const electronicProgramGuide: ElectronicProgramGuide = {
    channelName: 'channel',
    date: '2002-02-02',
    programs: []
  };

  const expectedAction = {
    payload: {
      ...electronicProgramGuide
    },
    type: 'EPG_LOADED',
  };
  expect(epgLoadedAction(electronicProgramGuide)).toEqual(expectedAction);
});

test('epgProgramSelectionChangedAction -- should create action', () => {

  const expectedAction = {
    payload: 'program-id',
    type: 'EPG_PROGRAM_SELECTION_CHANGED',
  };
  expect(epgProgramSelectionChangedAction('program-id')).toEqual(expectedAction);
});

let initialState: EpgStoreType;
beforeEach(() => {
  initialState = {
    selectedPrograms: []
  };
});

test('epgStore -- handle epgLoadedAction', () => {
  const electronicProgramGuide: ElectronicProgramGuide = {
    channelName: 'channel-3',
    date: '2003-03-03',
    programs: [{
      end: NaN,
      fileSize: 3003,
      id: '3',
      name: 'prog-3',
      start: 3003003
    }]
  };

  const action = epgLoadedAction(electronicProgramGuide);
  const result = epgStore(initialState, action);

  const expectedState: EpgStoreType = {
    electronicProgramGuide: {...electronicProgramGuide},
    selectedPrograms: []
  };
  expect(result).toEqual(expectedState);
});

test('epgStore -- handle epgProgramSelectionChangedAction, add program', () => {
  const action = epgProgramSelectionChangedAction('1');
  const oldState: EpgStoreType = {
    selectedPrograms: ['2']
  };
  const result = epgStore(oldState, action);

  const expectedState: EpgStoreType = {
    selectedPrograms: ['2', '1']
  };
  expect(result).toEqual(expectedState);
  expect(result.selectedPrograms).not.toBe(oldState.selectedPrograms); // todo: avoid multiple assertions in one test
});

test('epgStore -- handle epgProgramSelectionChangedAction, remove program', () => {
  const action = epgProgramSelectionChangedAction('4');
  const oldState: EpgStoreType = {
    selectedPrograms: ['3', '4', '5']
  };
  const result = epgStore(oldState, action);

  const expectedState: EpgStoreType = {
    selectedPrograms: ['3', '5']
  };
  expect(result).toEqual(expectedState);
  expect(oldState.selectedPrograms.length).toEqual(3); // todo: avoid multiple assertions in one test
});
