import * as enzyme from 'enzyme';
import * as React from 'react';
import EpgList from './EpgList';

test('EpgList -- should render with electronicProgramGuide', () => {
  const onSelection = (id: string) => undefined;
  const electronicProgramGuide: ElectronicProgramGuide = {
    channelName: 'first',
    date: '2000-01-02',
    programs: [{
      end: NaN,
      fileSize: 0,
      id: '101',
      name: '',
      start: 0,
    }]
  };

  const epgList = enzyme.shallow(<EpgList onProgramSelection={onSelection}
                                          electronicProgramGuide={electronicProgramGuide}/>);
  expect(epgList.find('.epg-list')).toBeDefined();
  expect(epgList).toMatchSnapshot();
});

test('EpgList -- should render without electronicProgramGuide', () => {
  const onSelection = (id: string) => undefined;
  const epgList = enzyme.shallow(<EpgList onProgramSelection={onSelection}/>);
  expect(epgList.find('.epg-list-empty').text()).toEqual('EPG is not loaded');
});
