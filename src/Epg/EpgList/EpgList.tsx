import * as React from 'react';

import './EpgList.css';
import EpgListItem from './EpgListItem';

interface EpgListProps {
  electronicProgramGuide?: ElectronicProgramGuide,
  onProgramSelection: (id: string) => void
}

const epgHeader = (electronicProgramGuide: ElectronicProgramGuide) => {
  const {channelName, date} = electronicProgramGuide;
  return (
    <div>
      <h4>{channelName}</h4>
      <h5>{(new Date(date)).toLocaleDateString()}</h5>
    </div>
  );
};

class EpgList extends React.PureComponent<EpgListProps> {

  public render() {
    const {electronicProgramGuide, onProgramSelection} = this.props;
    if (electronicProgramGuide === undefined) {
      return <div className="epg-list-empty">EPG is not loaded</div>
    }
    return (
      <div className="epg-list-container">
        {epgHeader(electronicProgramGuide)}
        <ul className="epg-list">
          <div className="epg-list-header">
            <span>Program title</span>
            <span>Show time</span>
          </div>
          {electronicProgramGuide.programs.map(program =>
            <EpgListItem
              key={program.id}
              program={program}
              onProgramSelection={onProgramSelection}
            />
          )}
        </ul>
      </div>
    );
  }
}

export default EpgList;
