import classNames from 'classnames';
import * as React from 'react';

import Timestamp from '../../common/Timestamp/Timestamp';

interface EpgListItemProps {
  program: Program,
  onProgramSelection: (id: string) => void
}

const EpgListItem: React.StatelessComponent<EpgListItemProps> = (props) => {
  const {program: {id, name, selected, start}, onProgramSelection} = props;
  const handleClick = (e: any) => {
    e.preventDefault();
    onProgramSelection(id);
  };
  return (
    <li onClick={handleClick}
        className={classNames('epg-list-item', {'selected': !!selected})}
    >
      <span>{name}</span>
      <div>
        <Timestamp date={new Date(start)}/>
      </div>
    </li>
  );
};

export default EpgListItem;
