interface ElectronicProgramGuide {
  date: string,
  channelName: string,
  programs: Program[]
}

interface Program {
  id: string,
  start: number, // todo: consider change to Date type
  end: number,
  name: string,
  fileSize: number,
  selected?: boolean
}

interface ElectronicProgramGuideSummary {
  selectedProgramsCount: number,
  totalSize: number,
  totalTime: number
}
