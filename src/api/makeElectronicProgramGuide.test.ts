import makeElectronicProgramGuide from "./makeElectronicProgramGuide";

test('makeElectronicProgramGuide -- should make', () => {
  const electronicProgramGuideDto: ElectronicProgramGuideDto = {
    channelName: 'first',
    date: '2018-09-01',
    programmes: [{
      fileSize: 100,
      id: 'id-100',
      name: 'name-100',
      start: 100100
    }]
  };

  const expected: ElectronicProgramGuide = {
    channelName: 'first',
    date: '2018-09-01',
    programs: [{
      end: NaN,
      fileSize: 100,
      id: 'id-100',
      name: 'name-100',
      start: 100100,
    }]
  };

  expect(makeElectronicProgramGuide(electronicProgramGuideDto)).toEqual(expected);
});

test('makeElectronicProgramGuide -- end date resolution by start date of next program', () => {
  const electronicProgramGuideDto: ElectronicProgramGuideDto = {
    channelName: 'first',
    date: '2018-09-01',
    programmes: [{
      fileSize: 0,
      id: '0',
      name: '',
      start: 0
    },{
      fileSize: 0,
      id: '0',
      name: '',
      start: 101
    }]
  };

  const result = makeElectronicProgramGuide(electronicProgramGuideDto);
  expect(result.programs[0].end).toEqual(101);
  expect(result.programs[1].end).toEqual(NaN);
});
