import baseUrl from './baseUrl';
import makeElectronicProgramGuide from './makeElectronicProgramGuide';

/*
const defaultElectronicProgramGuideDto: ElectronicProgramGuideDto = {
  'channelName': 'RTS',
  'date': '2018-09-05',
  'programmes': [
    {
      'fileSize': 28354000,
      'id': '2367276',
      'name': 'La Matinale en vidéo',
      'start': 1538715600000,
    },
    {
      'fileSize': 273540000,
      'id': '347329',
      'name': 'Top Models',
      'start': 1538719500000,
    },
    {
      'fileSize': 7243000,
      'id': '6546464',
      'name': 'C\'est ma question',
      'start': 1538721000000,
    },
    {
      'fileSize': 12735000,
      'id': '214213',
      'name': 'Un garçon solitaire',
      'start': 1538722500000,
    },
    {
      'fileSize': 5728000,
      'id': '643543',
      'name': 'Le court du jour',
      'start': 1538728200000,
    },
    {
      'fileSize': 16524000,
      'id': '9765754',
      'name': 'Les feux de l\'amour (7558)',
      'start': 1538728800000,
    }
  ]
};
*/

export const getElectronicProgramGuide = (): Promise<ElectronicProgramGuide> => {
  const url = `${baseUrl}epg.json`;
  return fetch(url)
  // todo: implement fetch wrapper and/or use Axios, add network and server errors generic handling
    .then(response => response.json()
      .then(json => makeElectronicProgramGuide(json as ElectronicProgramGuideDto))
    )
    .catch(error => {
      console.error('Failed to load EPG list.', error.message);
      return Promise.reject(new Error('Failed to load EPG list.' + error.message));
    });
};

export const submitWishList = (programs: string[]): Promise<void> => {
  const body = JSON.stringify(programs);
  console.info(`Selection submit call to back-end, payload: ${body}`);
  return Promise.resolve();
};
