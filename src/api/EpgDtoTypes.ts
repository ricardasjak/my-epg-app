interface ElectronicProgramGuideDto {
  date: string,
  channelName: string,
  programmes: ProgrammeDto[]
}

interface ProgrammeDto {
  id: string,
  start: number,
  end?: number, // end time or duration property is missing in back-end response
  name: string,
  fileSize: number
}
