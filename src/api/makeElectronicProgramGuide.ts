const resolveEndDate = (programmes: ProgrammeDto[], index: number) => {
  let { end } = programmes[index];
  if (end === undefined && Boolean(programmes[index + 1])) {
    // todo: this is a hack, but there is no other way how to estimate end time, default
    end = programmes[index + 1].start;
  }
  return end || NaN;
};

const makeElectronicProgramGuide = (epgListDto: ElectronicProgramGuideDto): ElectronicProgramGuide => {
  const {channelName, date, programmes} = epgListDto;
  return {
    channelName,
    date,
    programs: programmes.map(({id, name, fileSize, start, end}, index) => ({
        end: resolveEndDate(programmes, index),
        fileSize,
        id,
        name,
        start,
      })
    )
  }
};

export default makeElectronicProgramGuide;
